<?php

class Post extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'posts';

    public function member() {
        return $this->belongsTo('Member');
    }
}
