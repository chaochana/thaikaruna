<?php

class Document extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'documents';

    public function member() {
        return $this->belongsTo('Member');
    }
}
