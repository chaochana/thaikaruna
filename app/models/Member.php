<?php

class Member extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */

	protected $table = 'member';
	//public static $key = 'MemberID';
	public $primaryKey = 'MemberID';

	public function posts() {
        return $this->hasMany('Post');
    }

	public function photos() {
        return $this->hasMany('Photo');
    }

	public function documents() {
        return $this->hasMany('Documents');
    }    

	public function checkin() {
        return $this->hasMany('Checkin');
    }  

}