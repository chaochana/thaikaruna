<?php

class Checkin extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'checkin';

    public function member() {
        return $this->belongsTo('Member');
    }
}
