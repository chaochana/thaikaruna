<?php

class Tracking extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tracking';

    public function member() {
        return $this->belongsTo('Member');
    }
}
