<?php

class Photo extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'photos';

    public function member() {
        return $this->belongsTo('Member');
    }
}
