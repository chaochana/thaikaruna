<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::get('checkin', function()
{
	return View::make('checkin_search');
});

Route::get('participants', function()
{
	$participats = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

	return View::make('participants')->withParticipants($participats)->withGroup(' ');
});

Route::get('participants/group/{group}', function($group)
{
	$participats = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->where('Group', '=', $group)
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();
	$print_group = "- group ".$group;

	return View::make('participants')->withParticipants($participats)->withGroup($print_group);
});

Route::get('participants/nogroup', function()
{
	/*
	$participants = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->whereGroup(DB::raw('null'))
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();
	*/
	//$query = 'SELECT * FROM member, checkin WHERE member.MemberID = checkin.member_id AND member.Group = "" GROUP BY member.MemberID ORDER BY member.name';

	$participants = DB::select('SELECT * FROM member, checkin WHERE member.MemberID = checkin.member_id AND member.Group IS NULL GROUP BY member.MemberID ORDER BY member.name');

	//$participants = DB::query('SELECT * FROM member WHERE MemberID = "112" ');

	return View::make('participants')->withParticipants($participants)->withGroup(' - No Group');
});

Route::get('checkin/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('checkin')->withMember($member);
});

Route::post('/checkin/{id}', function($id)
{		
	$checkin = new Checkin();
    $checkin->member_id = $id;
    $checkin->save();

	return Redirect::to('checkin/'.$id);
});

Route::get('view', function()
{
	return View::make('view_search');
});

Route::get('view/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('view')->withMember($member);
});

Route::get('view/{id}/addgroup/{group}', function($id,$group)
{
	DB::table('member')
	    ->where('MemberID', '=', $id)
	    ->update(
	    	array('Group' => $group));	

	$member = Member::where('MemberID','LIKE',$id)->first();    
			
	return Redirect::to('view/'.$member->MemberID)->withMember($member);
});

Route::get('view/{id}/cleargroup', function($id)
{
	DB::table('member')
	    ->where('MemberID', '=', $id)
	    ->update(
	    	array('Group' => ''));	

	$member = Member::where('MemberID','LIKE',$id)->first();    
			
	return Redirect::to('view/'.$member->MemberID)->withMember($member);
});

Route::get('/newphoto/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('newphoto')->withMember($member);
});

Route::get('view/{member}/deltracking/{id}', function($member,$id)
{
	$affectedRows = Tracking::find($id)->delete();
	
	$member = Member::where('MemberID','LIKE',$member)->first();
	
	return Redirect::to('view/'.$member->MemberID)->withMember($member);
});

Route::get('view/{member}/delpost/{id}', function($member,$id)
{
	/*
	$post = Post::find($id)->first();
	
	$post->delete();
	*/
	$affectedRows = Post::find($id)->delete();
	
	$member = Member::where('MemberID','LIKE',$member)->first();
	
	return Redirect::to('view/'.$member->MemberID)->withMember($member);
});

Route::get('view/{member}/delphoto/{id}', function($member,$id)
{
	/*
	$post = Post::find($id)->first();
	
	$post->delete();
	*/
	$affectedRows = Photo::find($id)->delete();
	
	$member = Member::where('MemberID','LIKE',$member)->first();

	sleep(2);
	
	return Redirect::to('view/'.$member->MemberID)->withMember($member);
});

Route::post('/newphoto/{id}', function($id)
{

	$rules = array(
		'file' => 'image|max:5000'
	);
	$validation = Validator::make(Input::all(), $rules);
	if ($validation->fails())
	{
		return Redirect::to('newphoto/'.$id)->withErrors($validation)->withInput();
	}
	else
	{
		$file = Input::file('photo');
		$ext = $file->guessExtension();
		
		$datetime = date('YmdHi');

		$file->move('upload_photo', $datetime.'-'.$file->getClientOriginalName());

		$photo = new Photo();
        $photo->photo = './upload_photo/'.$datetime.'-'.$file->getClientOriginalName();
        $photo->photo_description = Input::file('photo_description');;
        $photo->member_id = $id;
        $photo->save();

		return Redirect::to('view/'.$id);
	}
});

Route::get('/newdoc/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('newdoc')->withMember($member);
});

Route::post('/newdoc/{id}', function($id)
{

	$rules = array(
		'file' => 'max:10000'
	);
	$validation = Validator::make(Input::all(), $rules);
	if ($validation->fails())
	{
		return Redirect::to('newdoc/'.$id)->withErrors($validation)->withInput();
	}
	else
	{
		$file = Input::file('file');
		$ext = $file->guessExtension();
		
		$datetime = date('YmdHi');

		$file->move('upload_document', $datetime.'-'.$file->getClientOriginalName());

		$document = new Document();
        $document->document = './upload_document/'.$datetime.'-'.$file->getClientOriginalName();
        $document->member_id = $id;
        $document->save();

		return Redirect::to('view/'.$id);
	}
});

Route::get('/newtracking/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('newtracking')->withMember($member);
});

Route::post('/newtracking/{id}', function($id)
{

	//dd(Input::get('tracking'));
	$rules = array(
		'overall_rating' => 'required'
	);
	$validation = Validator::make(Input::all(), $rules);
	if ($validation->fails())
	{
		return Redirect::to('newtracking/'.$id)->withErrors($validation)->withInput();
	}
	else
	{	
		if (Input::get('tracking') != ""){
			$tracking_items = implode(" ",Input::get('tracking'));
		} else {
			$tracking_items = "";
		}
		
		$tracking = new Tracking();
        $tracking->overall_rating = Input::get('overall_rating');
        $tracking->tracking = $tracking_items;
        $tracking->additional = Input::get('additional');
        $tracking->member_id = $id;
        $tracking->save();

		return Redirect::to('view/'.$id);
	}
});

Route::get('/newpost/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('newpost')->withMember($member);
});

Route::post('/newpost/{id}', function($id)
{

	$rules = array(
		'post' => 'required'
	);
	$validation = Validator::make(Input::all(), $rules);
	if ($validation->fails())
	{
		return Redirect::to('newpost/'.$id)->withErrors($validation)->withInput();
	}
	else
	{
		if ( Input::get('year') == "" ){
			$timeline = "";
		} else if ( Input::get('month') == "" ){
			$timeline = (Input::get('year')-543)."-00-00";
		} else if ( Input::get('day') == "" ){
			$timeline = (Input::get('year')-543)."-".Input::get('month')."-00";
		}  else {
			$timeline = (Input::get('year')-543)."-".Input::get('month')."-".Input::get('day');
		}
		
		
		$post = new Post();
        $post->post = Input::get('post');
        $post->post_by = Input::get('post_by');
        $post->member_id = $id;
		$post->timeline = $timeline;
        $post->save();

		return Redirect::to('view/'.$id);
	}
});

Route::get('/view/{member}/editpost/{id}', function($member,$id)
{
	$member = Member::where('MemberID','LIKE',$member)->first();
	
	$post = Post::find($id)->first();
			
	return View::make('editpost')->withMember($member)->withPost($post);
});

Route::post('/view/{$member}/editpost/{id}', function($member,$id)
{

	$rules = array(
		'post' => 'required'
	);
	$validation = Validator::make(Input::all(), $rules);
	if ($validation->fails())
	{
		return Redirect::to('view/'.$member.'/editpost/'.$id)->withErrors($validation)->withInput();
	}
	else
	{
		if ( Input::get('year') == "" ){
			$timeline = "";
		} else if ( Input::get('month') == "" ){
			$timeline = (Input::get('year')-543)."-00-00";
		} else if ( Input::get('day') == "" ){
			$timeline = (Input::get('year')-543)."-".Input::get('month')."-00";
		}  else {
			$timeline = (Input::get('year')-543)."-".Input::get('month')."-".Input::get('day');
		}
		
		$post = Post::find($id)->first();
        $post->post = Input::get('post');
        $post->post_by = Input::get('post_by');
        $post->member_id = $id;
		$post->timeline = $timeline;
        $post->save();

		return Redirect::to('view/'.$member);
	}
});


Route::get('/member', function()
{
	return View::make('member_search');
});

Route::get('/member/{id}', function($id)
{
	$member = Member::where('MemberID','LIKE',$id)->first();
			
	return View::make('member')->withMember($member);
});

Route::post('/member/{id}', function($id)
{
	$rules = array(
	'Name' => "required",
	'LastName' => "required",
	'CitizenID' => "required"
	);

	$validation = Validator::make(Input::all(), $rules);
	if ($validation->fails())
	{
		return Redirect::to('/member/'.$id)->withErrors($validation)->withInput();
	}

	$member = DB::table('member')->where('MemberID','=',$id)->first();

	$affected = DB::table('member')
    ->where('MemberID', '=', $id)
    ->update(
    	array('Title' => Input::get('Title'),
    	'Name' => Input::get('Name'),
    	'LastName' => Input::get('LastName'),
    	'CitizenID' => Input::get('CitizenID'),
    	'Gender' => Input::get('Gender'),
    	'BirthYear' => Input::get('BirthYear'),
    	'Symptom1' => Input::get('Symptom1'),
    	'Symptom2' => Input::get('Symptom2'),
    	'Symptom3' => Input::get('Symptom3'),
    	'Symptom4' => Input::get('Symptom4'),
    	'Symptom5' => Input::get('Symptom5'),
    	'Address' => Input::get('Address'),
    	'HomeTel' => Input::get('HomeTel'),
    	'Mobile' => Input::get('Mobile'),
    	'OfficeTel' => Input::get('OfficeTel'),
    	'Fax' => Input::get('Fax'),
    	'ContactPerson' => Input::get('ContactPerson'),
    	'ContactPersonTel' => Input::get('ContactPersonTel'),
    	'Surety' => Input::get('Surety'),
    	'Position' => Input::get('Position'),
    	'Department' => Input::get('Department'),
    	'ReportTo' => Input::get('ReportTo'),
       	'Note' => Input::get('Note'),
		'treatment_history' => Input::get('treatment_history')
    	));

	if ($affected == 1)
	{
		return Redirect::to('/member/'.$id)->with('notify','Information updated');
	}
	
	return Redirect::to('/member/'.$id)->withInput();
});

/*
Event::listen('illuminate.query', function() {
    print_r(func_get_args());
});
*/
