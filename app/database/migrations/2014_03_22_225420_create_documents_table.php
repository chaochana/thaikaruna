<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function($table)
			{
				$table->increments('id');
				$table->integer('member_id');
				$table->string('document');
				$table->string('document_description')->nullable();				
				$table->datetime('created_at');	
				$table->datetime('updated_at');					
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}
