<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracking', function($table)
			{
				$table->increments('id');
				$table->integer('member_id');				
				$table->integer('overall_rating');
				$table->text('tracking');
				$table->datetime('created_at');	
				$table->datetime('updated_at');					
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracking');
	}

}
