﻿<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>AnyFreelance</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <link rel="shortcut icon" href="{{ URL::asset('') }}img/favicon.ico"> 
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/foundation.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/foundation-icons.css') }}" />		
		<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
		
		@yield('css')

		<style type="text/css">
			.row {min-width:99% !important;}
		</style>
		
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}"/>
		
		<script src="js/modernizr.js"></script>

	</head>
	<body id="page">
		<div id="wrap">
			<div id="main">    		

					<div class="fixed">

						<nav class="top-bar data-topbar" style="background-color:#222222;color:#FFFFFF;">
						  <ul class="title-area">
							<li class="name">
							  <h1 class="font-lobster font-light"><a href="{{ URL::asset('') }}" style="color:#FFFFFF;">AnyFreelance</a></h1>
							</li>
							<li class="toggle-topbar menu-icon" style="background-color:#222222;color:#FFFFFF;"><a href="#">Menu</a></li>
						  </ul>
						
						  <section class="top-bar-section">
							<ul class="right">
							<!--
							  <li><a href="#" style="background-color:#222222;color:#FFFFFF;" class='font-helvetica font-large font-light'>hot-stuff</a></li>		
							  <li><a href="#" style="background-color:#222222;color:#FFFFFF;" class='font-helvetica font-large font-light'>feature-projects</a></li>	
							  <li><a href="#" style="background-color:#222222;color:#FFFFFF;" class='font-helvetica font-large font-light'>article</a></li>	
							  <li><a href="#" style="background-color:#222222;color:#FFFFFF;" class='font-helvetica font-large font-light'>designers</a></li>	
							  <li><a href="#" style="background-color:#222222;color:#FFFFFF;" class='font-helvetica font-large font-light'>hire</a></li>								  							  							  								
							  <li class="has-dropdown" style="background-color:#222222;color:#FFFFFF;">
								<a href="#" style="background-color:#222222;color:#FFFFFF;">explore</a>
								<ul class="dropdown">
								  <li><a href="#" style="background-color:#222222;color:#FFFFFF;">how it works</a></li>
								  <li><a href="#" style="background-color:#222222;color:#FFFFFF;">find and hire a freelance you impress</a></li>
								  <li><a href="#" style="background-color:#222222;color:#FFFFFF;">get insprired</a></li>
								</ul>
							  </li>
							 -->
							 <?php
							 	if (Auth::check())
								{
							?>
								<li><a href="#" style="background-color:#222222;color:#FFFFFF;">Calendar</a></li>

								<li><a href="#" style="background-color:#222222;color:#FFFFFF;">Projects</a></li>

								<li><a href="{{ URL::asset('') }}backyard/workpieces" style="background-color:#222222;color:#FFFFFF;">Workpieces</a></li>		
							 	<li>
							 		<a href="{{ URL::asset('') }}profile" style="background-color:#222222;color:#FFFFFF;">
									<img src="{{ URL::asset('headshot') }}/{{ Auth::user()->id }}.png" width='30'> 
							 		Profile
							 		</a>
							 	</li>
							 	<li>
							 	<a href="{{ URL::asset('') }}logout" style="background-color:#222222;color:#FFFFFF;">logout</a></li>	
							<?php
								}
								else
								{
							?>
								<li><a href="{{ URL::asset('') }}" style="background-color:#222222;color:#FFFFFF;">Recent Workpieces</a></li>
								<li><a href="{{ URL::asset('') }}registration" style="background-color:#222222;color:#FFFFFF;">Register</a></li>								 
								<li>
									<a href="{{ URL::asset('') }}login" style="background-color:#222222;color:#FFFFFF;">login
									</a>
								</li>	
							<?php
								}
							?>									
							 	
	

							</ul>	
						   </section>	
						</div>
				
				@yield('header')
				
				<div class="row">		
					
					@yield('content')
					
				</div><!-- End of Main Row -->
			</div><!-- End of class Main -->
		</div><!-- End of class wrapper -->		

		<div id="footer">
			<div class="row">
				<div class="large-8 medium-10 large-centered medium-centered columns">
					<center>
						<h2 class="font-lobster" style="color: #FFFFFF">AnyFreelance</h2>
					</center>
				</div>
			</div>
			<div class="row">
				<div class="large-8 medium-10 large-centered medium-centered columns">
					<div class="row">
						<div class="large-3 medium-3 columns">
							<center><h2 style="color:#FFFFFF"><i class="fi-lightbulb" class="font-medium"></i></h2></center>
							<p class="font-helvetica font-light font-smaller">
								Found a creative service that you really like. Get inspired and get lost in a ton of ideas.
							</p>
						</div>
						<div class="large-3 medium-3 columns">
							<center><h2 style="color:#FFFFFF"><i class="fi-credit-card"></i></h2></center>
							<p class="font-helvetica font-light font-smaller">
								Enable maximum benefit of your payment by Credit care or Paypal.
							</p>
						</div>
						<div class="large-3 medium-3 columns">
							<center><h2 style="color:#FFFFFF"><i class="fi-map"></i></h2></center>
							<p class="font-helvetica font-light font-smaller">
								Any detail will not be missed by provided tools
							</p>
						</div>			
						<div class="large-3 medium-3 columns">
							<center><h2 style="color:#FFFFFF"><i class="fi-torsos-all"></i></h2></center>
							<p class="font-helvetica font-light font-smaller">
								Freelancer & Buyer get Connected!
							</p>
						</div>																
					</div>		
				</div>
			</div>
			<div class="row">
				<div class="large-8 medium-10 large-centered medium-centered columns">
					<center><hr width='80%'><center>
					<center>
						<h5 style="color: #FFFFFF">
						<span class="font-lobster">AnyFreelance</span>
						is a product of cmd.io	
						</h5>
					</center>
				</div>
			</div>					
		</div>	
	

		<script src="{{ URL::asset('') }}js/foundation/foundation.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.clearing.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.dropdown.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.joyride.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.magellan.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.orbit.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.reveal.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.topbar.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.interchange.js"></script>

		<!--
		<script src="{{ URL::asset('') }}js/foundation/foundation.section.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.alerts.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.forms.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.cookie.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.tooltips.js"></script>
		-->

		@yield('js')

		<script>
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		  document.write('<script src="http://foundation.zurb.com/docs/assets/vendor/'
			+ ('__proto__' in {} ? 'zepto' : 'jquery')
			+ '.js"><\/script>');
	
		</script>

		<script src="http://foundation3.zurb.com/docs/assets/vendor/custom.modernizr.js"></script>

		<script src="http://foundation3.zurb.com/docs/assets/docs.js"></script>

		<script>
			$(document).foundation();

			// For Kitchen Sink Page
			$('#start-jr').on('click', function() {
			$(document).foundation('joyride','start');
			});
		</script>				

	</body>
</html>