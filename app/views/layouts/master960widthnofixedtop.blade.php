<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>ไทยกรุณา</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <link rel="shortcut icon" href="{{ URL::asset('') }}img/favicon.ico"> 
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/foundation.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/foundation-icons.css') }}" />		
		<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

		@yield('js')
		
		@yield('css')
		
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}"/>
		
		<script src="{{ URL::asset('') }}js/modernizr.js"></script>

	</head>
	<body id="page">
		<div id="wrap">
			<div id="main">    		
				
				<div class="row">		
					
					<div class="fixed">

						<nav class="top-bar data-topbar" style="background-color:#222222;color:#FFFFFF;">
						  <ul class="title-area" style="background-color:#222222;color:#FFFFFF;">
							<li class="name">
							  <h1 class="font-lobster font-light"><a href="{{ URL::asset('') }}" style="color:#FFFFFF;">ไทยกรุณา</a></h1>
							</li>
							<li class="toggle-topbar menu-icon" style="background-color:#222222;color:#FFFFFF;"><a href="#">เมนู</a></li>
						  </ul>
						
						  <section class="top-bar-section" style="background-color:#222222;color:#FFFFFF;">
							<ul class="right">
								<!--
								<li><a href="{{ URL::asset('') }}view" style="background-color:#222222;color:#FFFFFF;">View</a></li>

								<li><a href="{{ URL::asset('') }}member" style="background-color:#222222;color:#FFFFFF;">Information Update</a></li>	
								-->						 
							</ul>	
						   </section>	
						</nav>
						</div>

					@yield('header')

					@yield('content')
					
				</div><!-- End of Main Row -->
			</div><!-- End of class Main -->
		</div><!-- End of class wrapper -->		

		<div id="footer">
			<div class="row">
				<div class="large-8 medium-10 large-centered medium-centered columns">
					<center>
						<h2 class="font-lobster" style="color: #FFFFFF">ไทยกรุณา</h2>
					</center>
				</div>
			</div>
			<div class="row">
			</div>
			<div class="row">
			</div>					
		</div>	
		

		<script src="{{ URL::asset('') }}js/foundation/foundation.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.clearing.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.dropdown.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.joyride.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.magellan.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.orbit.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.reveal.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.topbar.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.interchange.js"></script>

		<!--
		<script src="{{ URL::asset('') }}js/foundation/foundation.section.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.alerts.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.forms.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.cookie.js"></script>
		<script src="{{ URL::asset('') }}js/foundation/foundation.tooltips.js"></script>
		-->
		<script>
		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		  document.write('<script src="http://foundation.zurb.com/docs/assets/vendor/'
			+ ('__proto__' in {} ? 'zepto' : 'jquery')
			+ '.js"><\/script>');
	
		</script>

		<script src="http://foundation3.zurb.com/docs/assets/vendor/custom.modernizr.js"></script>

		<script src="http://foundation3.zurb.com/docs/assets/docs.js"></script>

		<script>
			$(document).foundation();

			// For Kitchen Sink Page
			$('#start-jr').on('click', function() {
			$(document).foundation('joyride','start');
			});
		</script>				

	</body>
</html>