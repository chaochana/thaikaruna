@extends('layouts.master960widthnofixedtop')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:20px;">
		<div class="large-6 medium-8 small-10 large-centered medium-centered small-centered columns" style="max-width:960px">
		
		<?= Form::open(array('method' => 'get')) ?>
		<?= Form::label('member_search', 'ค้นหาสมาชิก: ') ?>
		<?= Form::text('member_search','',array('placeholder'=>'ระบุรหัส ชื่อ หรือ นามสกุล')) ?>
		<?= Form::submit('ค้นหา', array('class'=>'small expand button')) ?>		
		<?= Form::close() ?>

		<?php
			if (!(is_null($_GET['member_search']))) {
				$member_search = $_GET['member_search'];

				if ($member_search != ""){

					if ( preg_match("/[0-9]/i",$member_search) ){
						$member = Member::where('MemberID','LIKE',$member_search)->first();

						if(is_null($member)){
							echo $member_search." not match any record";
						} else {
							echo '<u><a href="'.URL::asset('').'member/'.$member->MemberID.'">'.$member->Name." ".$member->LastName." ( ".$member->MemberID." )</a></u><br/>";							
						}

					}else{
						$member_search = "%".$member_search."%";
						$members = Member::where('Name','LIKE',$member_search)->where('LastName','LIKE',$member_search,'OR')->get();


						if(is_null($members)){
							echo $member_search." not match any record";
						} else {
							echo "<ul>";
							foreach ($members as $member) {
								echo '<li><u><a href="'.URL::asset('').'member/'.$member->MemberID.'">'.$member->Name." ".$member->LastName." ( ".$member->MemberID." )</a></u></li>";
							}			
							echo "</ul>";				
						}

					}

					//dd($member);
				}

			}

		?>

		</div>  

	</div> 
	<br/>

@stop