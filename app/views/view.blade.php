@extends('layouts.master960width')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:20px;">
		<div class="large-8 medium-10 small-10 large-centered medium-centered small-centered columns" style="max-width:960px">
			<h1>{{ $member->Title." ".$member->Name." ".$member->LastName }}</h1>

			<ol>
				<li>1. {{ $member->Symptom1 }}</li>
				<li>2. {{ $member->Symptom2 }}</li>
				<li>3. {{ $member->Symptom3 }}</li>
				<li>4. {{ $member->Symptom4 }}</li>
				<li>5. {{ $member->Symptom5 }}</li>									
			</ol>
			<br/>
			<?php
			$a = "color:#dedede";
			$b = "color:#dedede";
			$c = "color:#dedede";
			switch ($member->Group) {
				case 'a':
					$a = "color:black";
					break;
				case 'b':
					$b = "color:black";
					break;
				case 'c':
					$c = "color:black";
					break;														
				default:
					break;
			}
			?>
			<h2>Group: 
			<a href='{{ URL::asset('') }}view/{{ $member->MemberID }}/addgroup/a' style='{{ $a }}' onclick="return confirm(&quot;แน่ใจว่านะ?&quot;)">[A]</a> 
			<a href='{{ URL::asset('') }}view/{{ $member->MemberID }}/addgroup/b' style='{{ $b }}' onclick="return confirm(&quot;แน่ใจว่านะ?&quot;)">[B]</a> 
			<a href='{{ URL::asset('') }}view/{{ $member->MemberID }}/addgroup/c' style='{{ $c }}' onclick="return confirm(&quot;แน่ใจว่านะ?&quot;)">[C]</a> 
			<a href='{{ URL::asset('') }}view/{{ $member->MemberID }}/cleargroup' style='color:red' onclick="return confirm(&quot;แน่ใจว่านะ?&quot;)">clear</a> 
			</h2>
			<br/>
			<h2><i class='fi-graph-trend'></i> Health Tracking <a href="{{ URL::asset('') }}newtracking/{{ $member->MemberID }}">+</a></h2>
			<?php
				$tracking_items = DB::table('tracking')->where('member_id','=',$member->MemberID)->orderBy('created_at', 'DESC')->get();

				foreach ($tracking_items as $tracking) {

					$date = strtotime($tracking->created_at);
					$print_date = date("d M Y",$date);			
					
					echo "<div class='panel'>";
					echo "[".$print_date."] ";
					echo "สุขภาพโดยรวม = ".$tracking->overall_rating."<br/><br/>";
					echo "อาการ การเจ็บป่วย: ".$tracking->tracking."<br/><br/>";
					echo "<textarea style='border:1px solid #696969'>".$tracking->additional."</textarea>";
					echo "<a href='".URL::asset('')."view/".$member->MemberID."/deltracking/".$tracking->id."' onclick='return confirm(\"แน่ใจว่าจะลบ?\")' class='right'><i class='fi-x' style='color:red;font-size:24px;'></i></a>";
					echo "<br/></div>";
					echo "<br/>";
					
				}
			?>

			<h2><i class='fi-comments'></i> Post <a href="{{ URL::asset('') }}newpost/{{ $member->MemberID }}">+</a></h2>
			<?php
				$posts = DB::table('posts')->where('member_id','=',$member->MemberID)->orderBy('timeline', 'DESC')->get();

				foreach ($posts as $post) {

					$date = strtotime($post->created_at);
					$print_date = date("d M Y",$date);

					$timeline = strtotime($post->timeline);
					$print_timeline = date("d M Y",$timeline);					
					
					echo "<div class='panel'>";
					echo "<h5>* ".$print_timeline."</h5>";
					echo "<textarea style='border:1px solid #696969'>".$post->post."</textarea>";
					if ($post->post_by = ""){
						$post_by = "...";
					} else {
						$post_by = $post->post_by;
					}
					echo "<p class='font-smaller'>Posted by ".$post_by." on ".$print_date."</p>";
					echo "<div class='right'>";				
					//echo "<a href='".URL::asset('')."view/".$member->MemberID."/editpost/".$post->id."'><i class='fi-page-edit' style='color:green;font-size:24px;'></i></a>"; 
					//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					echo "<a href='".URL::asset('')."view/".$member->MemberID."/delpost/".$post->id."' onclick='return confirm(\"แน่ใจว่าจะลบ?\")'><i class='fi-page-delete' style='color:red;font-size:24px;'></i></a>";
					echo "</div>";
					echo "<br/>";
					echo "</div>";
					echo "<br/>";
					
				}
			?>
			<br/>
			<h2><i class='fi-photo'></i> Photo <a href="{{ URL::asset('') }}newphoto/{{ $member->MemberID }}">+</a></h2>
			<?php
				$photos = DB::table('photos')->where('member_id','=',$member->MemberID)->orderBy('created_at', 'DESC')->get();
			?>

			<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4 clearing-thumbs" data-clearing>
			<?php
				foreach ($photos as $photo) {

					$date = strtotime($photo->created_at);
					$print_date = date("d M Y",$date);

					echo "<li><a href=".URL::asset('').$photo->photo."><img src='".URL::asset('').$photo->photo."'></a><font class='font-smaller'>".$print_date."</font><a href='".URL::asset('')."view/".$member->MemberID."/delphoto/".$photo->id."' onclick='return confirm(\"แน่ใจว่าจะลบ?\")'><font class='font-smaller right' style='color:red'>X</font></a></li>";
				}
			?>
			</ul>
			<br/>
			<h2><i class='fi-page'></i> Document <a href="{{ URL::asset('') }}newdoc/{{ $member->MemberID }}">+</a></h2>
			<?php
				$documents = DB::table('documents')->where('member_id','=',$member->MemberID)->orderBy('created_at', 'DESC')->get();
			?>

			<ol>
			<?php
				foreach ($documents as $document) {

					$date = strtotime($document->created_at);
					$print_date = date("d M Y",$date);

					echo "<li>[".$print_date."]<a href='".URL::asset('').$document->document."'>".$document->document."</a></li>";
				}
			?>
			</ol>
			<hr>
			<center><h1>NEW</h1></center>
			<div class="row">
					<div class="large-4 medium-4 columns">
						<center><h1><a href="{{ URL::asset('') }}newpost/{{ $member->MemberID }}"><i class='fi-comments'></i></a></h1>Post</center>
					</div>
					<div class="large-4 medium-4 columns">
						<center><h1><a href="{{ URL::asset('') }}newphoto/{{ $member->MemberID }}"><i class='fi-photo'></i></a></h1>Photo</center>
					</div>
					<div class="large-4 medium-4 columns">
						<center><h1><a href="{{ URL::asset('') }}newdoc/{{ $member->MemberID }}"><i class='fi-page'></i></a></h1>Document</center>
					</div>							
			</div>
		</div>  
		<br/>
	</div> 

@stop