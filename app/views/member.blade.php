@extends('layouts.master960width')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:40px;">
		<div class="large-8 medium-10 small-10 large-centered medium-centered small-centered columns" style="max-width:960px">
		<h3>{{ $member->Title." ".$member->Name." ".$member->LastName }}</h3>
		<?php $messages = $errors->all('<p style="color:red">:message</p>') ?>
		<?php foreach ($messages as $msg): ?>
			<?= $msg ?>
		<?php endforeach; ?>		
		<?= Form::open() ?>	
		<div class="row">
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('Title', 'คำนำหน้า: ') ?>
				<?= Form::text('Title', $member->Title ) ?>			
			</div>
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('Name', 'ชื่อ: ') ?>
				<?= Form::text('Name', $member->Name ) ?>			
			</div>
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('LastName', 'นามสกุล: ') ?>
				<?= Form::text('LastName', $member->LastName ) ?>			
			</div>			
		</div>	

		<div class="row">
			<div class="large-4 medium-4  small-12 columns">
				<?= Form::label('CitizenID', 'เลขบัตรประชาชน: ') ?>
				<?= Form::text('CitizenID', $member->CitizenID ) ?>	
			</div>
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('Gender', 'เพศ: ') ?>
				<?= Form::text('Gender', $member->Gender ) ?>		
			</div>		
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('BirthYear', 'BirthYear: ') ?>
				<?= Form::text('BirthYear', $member->BirthYear ) ?>	
			</div>				
		</div>

		<div class="row">
			<div class="large-6 medium-6  small-12 columns">
				วันที่สมัคร: {{ $member->DateApply }}
			</div>
			<div class="large-6 medium-6 small-12 columns">
				วันที่ครบห้าครั้ง: {{ $member->DateQualified }}	
			</div>					
		</div>		
		<br/>
		<div class="row">
			<div class="large-12 medium-12  small-12  columns">
				<?= Form::label('Symptom1', 'อาการ1: ') ?>
				<?= Form::text('Symptom1', $member->Symptom1 ) ?>	
			</div>	
		</div>

		<div class="row">
			<div class="large-12 medium-12  small-12 columns">
				<?= Form::label('Symptom2', 'อาการ2: ') ?>
				<?= Form::text('Symptom2', $member->Symptom2 ) ?>	
			</div>	
		</div>

		<div class="row">
			<div class="large-12 medium-12  small-12 columns">
				<?= Form::label('Symptom3', 'อาการ3: ') ?>
				<?= Form::text('Symptom3', $member->Symptom3 ) ?>	
			</div>	
		</div>

		<div class="row">
			<div class="large-12 medium-12  small-12 columns">
				<?= Form::label('Symptom4', 'อาการ4: ') ?>
				<?= Form::text('Symptom4', $member->Symptom4 ) ?>	
			</div>	
		</div>

		<div class="row">
			<div class="large-12 medium-12  small-12 columns">
				<?= Form::label('Symptom5', 'อาการ5: ') ?>
				<?= Form::text('Symptom5', $member->Symptom5 ) ?>	
			</div>	
		</div>		

		<div class="row">
			<div class="large-12 medium-12  small-12 columns">
				<?= Form::label('Address', 'ที่อยู่: ') ?>
				<?= Form::text('Address', $member->Address ) ?>	
			</div>	
		</div>	

		<div class="row">
			<div class="large-3 medium-3 small-6 columns">
				<?= Form::label('HomeTel', 'เบอร์โทรศัพท์บ้าน: ') ?>
				<?= Form::text('HomeTel', $member->HomeTel ) ?>	
			</div>	
			<div class="large-3 medium-3 small-6 columns">
				<?= Form::label('OfficeTel', 'เบอร์โทรศัพท์ที่ทำงาน: ') ?>
				<?= Form::text('OfficeTel', $member->OfficeTel ) ?>	
			</div>			
			<div class="large-3 medium-3 small-6 columns">
				<?= Form::label('Mobile', 'เบอร์โทรศัพท์มือถือ: ') ?>
				<?= Form::text('Mobile', $member->Mobile ) ?>	
			</div>	
			<div class="large-3 medium-3 small-6 columns">
				<?= Form::label('Fax', 'เบอร์แฟกส์: ') ?>
				<?= Form::text('Fax', $member->Fax ) ?>	
			</div>	
		</div>	

		<div class="row">
			<div class="large-6 medium-6 small-6 columns">
				<?= Form::label('ContactPerson', 'บุคคลที่สามารถติดต่อได้: ') ?>
				<?= Form::text('ContactPerson', $member->ContactPerson ) ?>	
			</div>	
			<div class="large-6 medium-6 small-6 columns">
				<?= Form::label('ContactPersonTel', 'เบอร์โทรศัพท์บุคคลติดต่อ: ') ?>
				<?= Form::text('ContactPersonTel', $member->ContactPersonTel ) ?>	
			</div>			
		</div>	

		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<?= Form::label('Surety', 'บุคคลรับรอง: ') ?>
				<?= Form::text('Surety', $member->Surety ) ?>	
			</div>			
		</div>	

		<div class="row">
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('Position', 'ตำแหน่งจิดอาสา: ') ?>
				<?= Form::text('Position', $member->Position ) ?>	
			</div>	
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('Department', 'งานจิดอาสา: ') ?>
				<?= Form::text('Department', $member->Department ) ?>	
			</div>	
			<div class="large-4 medium-4 small-12 columns">
				<?= Form::label('ReportTo', 'หัวหน้างาน: ') ?>
				<?= Form::text('ReportTo', $member->ReportTo ) ?>	
			</div>									
		</div>		

		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<?= Form::label('Note', 'บันทึก: ') ?>
				<?= Form::textarea('Note', $member->Note ) ?>	
			</div>			
		</div>	
		<br/>
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<?= Form::label('treatment_history', 'ประวัติการรักษา: ') ?>
				<?= Form::textarea('treatment_history', $member->treatment_history ) ?>	
			</div>			
		</div>			
		
		<br/>
		
		<?= Form::submit('ปรับปรุงข้อมูล', array('class'=>'small expand button')) ?>		
		<?= Form::close() ?>

		</div>  

	</div> 

@stop