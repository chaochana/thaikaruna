@extends('layouts.master960width')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:20px;">
		<div class="large-8 medium-10 small-10 large-centered medium-centered small-centered columns" style="max-width:960px">
			<h1>{{ $member->Title." ".$member->Name." ".$member->LastName }}</h1>

			<h3>New Photo</h3>
			<?php $messages = $errors->all('<p style="color:red">:message</p>') ?>
			<?php foreach ($messages as $msg): ?>
				<?= $msg ?>
			<?php endforeach; ?>
			<?= Form::open(array('files' => TRUE)) ?>
        	<?= Form::file('photo', array("placeholder"=>"Your workpiece","type"=>'file','accept'=>'image/*','capture'=>'camera')) ?>			
			<!--<input name='file' type="file" accept="image/*" capture="camera">-->		
			<?= Form::submit('Upload!', array('class'=>'small button')) ?>
			<?= Form::close() ?>			
		</div>  
		<br/>
	</div> 

@stop