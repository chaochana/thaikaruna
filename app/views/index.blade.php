@extends('layouts.masterfullwidth')

@section('css')
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">

	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	<!--
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop

@section('content')

	<div class="large-12 columns">
		<div class="row">
			<!--<div class="large-2 medium-4 columns hide-for-small-only" id="floatdiv" style="text-align:right;position:absolute;top:20px;left:30px;z-index:100">-->
			<div class="large-2 medium-3 columns" style="text-align:right;">			
				<h2 class="font-light"><a href="{{ URL::asset('') }}" style="color:#222222;">ไทยกรุณา</a></h2>
				
				<br/>
				<h4 style="font-weight:500">Cancer Session</h4>
				<br/>
				<h5><a href="{{ URL::asset('') }}checkin?member_search=">Check-in +</a></h5>	
				<h5><a href="{{ URL::asset('') }}member?member_search=">Update Personal Info +</a></h5>	
				<h5><a href="{{ URL::asset('') }}treatment?member_search=">View Treatment Info +</a></h5>					
				<h5><a href="{{ URL::asset('') }}view?member_search=">View +</a></h5>	
				<br/>
				<h5><a href="{{ URL::asset('') }}search">Search</a></h5>	
				<br/>				
				<hr/>

				<?php
					$participants = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_all = count($participants);

				    $participants_a = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->where('Group', '=', 'a')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_a = count($participants_a);	
				    
				    $participants_b = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->where('Group', '=', 'b')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_b = count($participants_b);

				    $participants_c = DB::table('member')
				        ->join('checkin', 'member.MemberID', '=', 'checkin.member_id')
				        ->where('Group', '=', 'c')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_c = count($participants_c);	

				    $participants_no = DB::select('SELECT * FROM member, checkin WHERE member.MemberID = checkin.member_id AND member.Group IS NULL GROUP BY member.MemberID ORDER BY member.name');

				    $num_no = count($participants_no);				    				    			        
				?>

				<h5><a href="{{ URL::asset('') }}participants">List of All Participants ({{ $num_all }})</a></h5>				
				<br/>
				<h5><a href="{{ URL::asset('') }}participants/group/a">Group A ({{ $num_a }})</a></h5>	
				<h5><a href="{{ URL::asset('') }}participants/group/b">Group B ({{ $num_b }})</a></h5>	
				<h5><a href="{{ URL::asset('') }}participants/group/c">Group C ({{ $num_c }})</a></h5>
				<h5><a href="{{ URL::asset('') }}participants/nogroup">No Group ({{ $num_no }})</a></h5>						
				<hr>

				<?php

				    $participants_interviewed = DB::table('member')
				        ->where('InterviewGroup', '=', 'interviewed')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_interviewed = count($participants_interviewed);	
				    
				    $participants_waitinginterview = DB::table('member')
				        ->where('InterviewGroup', '=', 'waitinginterview')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_waitinginterview = count($participants_waitinginterview);

				    $participants_nointerview = DB::table('member')
				        ->where('InterviewGroup', '=', 'nointerview')
				        ->groupBy('member.MemberID')
				        ->orderBy('member.Name')
				        ->get();

				    $num_nointerview = count($participants_nointerview);	  				    			        
				?>

				<h5>Success Interview Case</h5>
				<h5><a href="{{ URL::asset('') }}participants/interviewgroup/interviewed">สัมภาษณ์แล้ว ({{ $num_interviewed }})</a></h5>					
				<h5><a href="{{ URL::asset('') }}participants/interviewgroup/waitinginterview">รอสัมภาษณ์ ({{ $num_waitinginterview }})</a></h5>	
				<h5><a href="{{ URL::asset('') }}participants/interviewgroup/nointerview">ไม่เข้าเกณฑ์สัมภาษณ์ ({{ $num_nointerview }})</a></h5>					
				
			</div>
			<div class="large-4 medium-3 columns">

				<?php
					$tracking_items = DB::table('tracking')
								->join('member', 'tracking.member_id', '=', 'member.MemberID')
								->select('member.MemberID as ID', 'member.Name as Name', 'member.LastName as LastName',DB::raw("MAX(tracking.created_at) AS created_at"))
								->groupBy('member.MemberID')
								->orderBy('tracking.created_at', 'DESC')
								->get();

				    $num_tracked = count($tracking_items);				
				?>			
			
				<center><h3 style="font-weight:500">Tracked(<?= $num_tracked ?>)</h3></center>
				<?php 
					//$checkins = Checkin::with('member')->orderBy('created_at', 'DESC')->get(); 	

				  /*select m.Name as Name, m.LastName as LastName, t.overall_rating as Overall_Rating, MAX(t.created_at) as Created_At
					FROM member m, tracking t
					WHERE m.MemberID = t.member_id
					GROUP BY t.member_id*/

				  /*$tracking_items = DB::table('tracking')
								->join('member', 'tracking.member_id', '=', 'member.MemberID')
								->select('member.MemberID as ID', 'member.Name as Name', 'member.LastName as LastName','tracking.overall_rating as overall_rating',DB::raw('MAX(tracking.created_at) as created_at'))
								->groupBy('member.MemberID')
								//->orderBy('tracking.created_at', 'DESC')
								->get();*/

					$query = 'SELECT m.MemberID as ID, m.Name as Name, m.LastName as LastName, t.overall_rating as overall_rating, MAX(t.created_at) as created_at ';
					$query .= 'FROM member m, tracking t ';
					$query .= 'WHERE m.MemberID = t.member_id ';
					$query .= 'GROUP BY t.member_id';

					$tracking_items = DB::select( DB::raw($query) );
				?>		
				<ul>

				@foreach ( $tracking_items as $tracking )
				<?php
					$date = strtotime($tracking->created_at) + 25200;
					$print_date = date("D-d-M",$date);

					switch ($tracking->overall_rating) {
						case "1": $color = "rgba(198, 15, 19,0.5)";
						          break;
						case "2": $color = "rgba(198, 15, 19,0.4)";
						          break;
						case "3": $color = "rgba(198, 15, 19,0.3)";
						          break;
						case "4": $color = "rgba(198, 15, 19,0.2)";
						          break;
						case "5": $color = "rgba(198, 15, 19,0.1)";
						          break;						          						          						          						          
						case "6": $color = "rgba(93, 164, 35,0.1)";
						          break;						          
						case "7": $color = "rgba(93, 164, 35,0.2)";
						          break;
						case "8": $color = "rgba(93, 164, 35,0.3)";
						          break;						          
						case "9": $color = "rgba(93, 164, 35,0.4)";
						          break;
						case "10": $color = "rgba(93, 164, 35,0.5)";
						          break;
						default: $color = "rgba(255,255,255,0.2) ";
						         break;
					}

					echo "<li style='background-color:".$color."'>".$print_date." <a href='".URL::asset('')."view/".$tracking->ID."'><u>".$tracking->Name." ".$tracking->LastName." (".$tracking->ID.") <b>[".$tracking->overall_rating."]</b></u></a></li>";
				?>
				
				@endforeach
				</ul>			
				<hr/>
				<center><h3 style="font-weight:500">Checked-in</h3></center>
				<?php 
					//$checkins = Checkin::with('member')->orderBy('created_at', 'DESC')->get(); 			
					$checkins = DB::table('checkin')
								->join('member', 'checkin.member_id', '=', 'member.MemberID')
								->select('member.MemberID as ID', 'member.Name as Name', 'member.LastName as LastName','checkin.created_at')
								->orderBy('created_at', 'DESC')
								->get();
				?>		
				<ul>

				@foreach ( $checkins as $checkin )
				<?php
					$date = strtotime($checkin->created_at) + 25200;
					//$print_date = date("D-d-M H:i:s",$date);
					$print_date = date("D-d-M",$date);

					echo "<li>".$print_date." <a href='".URL::asset('')."view/".$checkin->ID."'><u>".$checkin->Name." ".$checkin->LastName." (".$checkin->ID.")</u></a></li>";
				?>
				
				@endforeach
				</ul>				
			</div>
			<div class="large-6 medium-6 columns" style='text-align: center;'>
				<div class="row">
					<h3 style="font-weight:500">Recent Photos</h3>
					<?php $photos = Photo::with('member')->orderBy('created_at', 'DESC')->take(20)->get(); ?>				
					<ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4" style="padding:0;margin:0">
					<?php
						foreach ($photos as $photo) {

							$date = strtotime($photo->created_at);
							$print_date = date("D d M Y",$date);

							echo "<li><a href='".URL::asset('').$photo->photo."'><img src='".URL::asset('').$photo->photo."' class='th'></a><font class='font-smallest'><u><a href='".URL::asset('')."view/".$photo->member->MemberID."'>".$photo->member->Name." ".$photo->member->LastName." (".$photo->member->MemberID.")</a></u><br/>"." ".$print_date."</font></li>\n";
						}
					?>
					</ul>	
				</div>			
			</div>					
		</div>
	</div>

@stop

