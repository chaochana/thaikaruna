@extends('layouts.master960width')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:20px;">
		<div class="large-8 medium-10 small-10 large-centered medium-centered small-centered columns" style="max-width:960px">
			<h1>{{ $member->Title." ".$member->Name." ".$member->LastName }}</h1>

			<h3>New Post</h3>
			<?php $messages = $errors->all('<p style="color:red">:message</p>') ?>
			<?php foreach ($messages as $msg): ?>
				<?= $msg ?>
			<?php endforeach; ?>
			<?= Form::open() ?>
			<?= Form::textarea('post', '', array('placeholder' => 'Content') ) ?>
			<br/>
			
			<?php
				$month_arr=array(   
					"1"=>"มกราคม",  
					"2"=>"กุมภาพันธ์",  
					"3"=>"มีนาคม",  
					"4"=>"เมษายน",  
					"5"=>"พฤษภาคม",  
					"6"=>"มิถุนายน",   
					"7"=>"กรกฎาคม",  
					"8"=>"สิงหาคม",  
					"9"=>"กันยายน",  
					"10"=>"ตุลาคม",  
					"11"=>"พฤศจิกายน",  
					"12"=>"ธันวาคม"                    
				);			
				$day_arr=array(  
					"1"=>"1",  
					"2"=>"2",  
					"3"=>"3",  
					"4"=>"4",  
					"5"=>"5",  
					"6"=>"6",   
					"7"=>"7",  
					"8"=>"8",  
					"9"=>"9",  
					"10"=>"10",  
					"11"=>"11",  
					"12"=>"12",     
					"13"=>"13",  
					"14"=>"14",  
					"15"=>"15",  
					"16"=>"16",   
					"17"=>"17",  
					"18"=>"18",  
					"19"=>"19",  
					"20"=>"20",  					
					"21"=>"21",  
					"22"=>"22",     
					"23"=>"23",  
					"24"=>"24",  
					"25"=>"25",  
					"26"=>"26",   
					"27"=>"27",  
					"28"=>"28",  
					"29"=>"29",  
					"30"=>"30",
					"31"=>"31"
				);
				$year_arr=array(  
					""=>"",
					"2557"=>"2557",  
					"2556"=>"2556",  
					"2555"=>"2555",  
					"2554"=>"2554",  
					"2553"=>"2553",  
					"2552"=>"2552",   
					"2551"=>"2551",  
					"2550"=>"2550",  					
					"2549"=>"2549",  
					"2548"=>"2548",  					
					"2547"=>"2547",  
					"2546"=>"2546",  
					"2545"=>"2545",  
					"2544"=>"2544",  
					"2543"=>"2543",  
					"2542"=>"2542",   
					"2541"=>"2541",  
					"2540"=>"2540",  	
					"2539"=>"2539",  
					"2538"=>"2538",  					
					"2537"=>"2537",  
					"2536"=>"2536",  
					"2535"=>"2535",  
					"2534"=>"2534",  
					"2533"=>"2533",  
					"2532"=>"2532",   
					"2531"=>"2531",  
					"2530"=>"2530",  
					"2529"=>"2529",  
					"2528"=>"2528",  					
					"2527"=>"2527",  
					"2526"=>"2526",  
					"2525"=>"2525",  
					"2524"=>"2524",  
					"2523"=>"2523",  
					"2522"=>"2522",   
					"2521"=>"2521",  
					"2520"=>"2520", 
					"2519"=>"2519",  
					"2518"=>"2518",  					
					"2517"=>"2517",  
					"2516"=>"2516",  
					"2515"=>"2515",  
					"2514"=>"2514",  
					"2513"=>"2513",  
					"2512"=>"2512",   
					"2511"=>"2511",  
					"2510"=>"2510"					
				);				
			?>			
			<?= Form::label('Timeline', 'Timeline: ') ?>		
			<div class='row'>
				<div class='large-4 medium-4 small-4 columns'>
					<?= Form::select('day', $day_arr) ?>
				</div>
				<div class='large-4 medium-4 small-4 columns'>
					<?= Form::select('month', $month_arr) ?>
				</div>			
				<div class='large-4 medium-4 small-4 columns'>
					<?= Form::select('year', $year_arr) ?>
				</div>			
			</div>
			<br/>
			<?= Form::label('post_by', 'Post by: ') ?>
			<?= Form::text('post_by', '', array('placeholder' => 'Post by')) ?>	
			<?= Form::submit('Post!', array('class'=>'small button')) ?>
			<?= Form::close() ?>			
		</div>  
		<br/>
	</div> 

@stop