@extends('layouts.master960width')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:20px;">
		<div class="large-8 medium-10 small-10 large-centered medium-centered small-centered columns" style="max-width:960px">
			<h1>{{ $member->Title." ".$member->Name." ".$member->LastName }}</h1>

			<h3>New Tracking</h3>
			<br/>
			<?php $messages = $errors->all('<p style="color:red">:message</p>') ?>
			<?php foreach ($messages as $msg): ?>
				<?= $msg ?>
			<?php endforeach; ?>
			<?= Form::open() ?>
			ประเมินภาพรวมสุขภาพ 10=ดีมาก 1=ไม่ดี เทียบกับครั้งก่อนถ้ามี:
			<?= Form::selectRange('overall_rating', 10, 1); ?>	
			อาการ-การเจ็บป่วย:<br/><br/>
			<input name='tracking[]' type="checkbox" value="แผลเปิด" /> แผลเปิด / 
			<input name='tracking[]' type="checkbox" value="เจ็บปวดแผล" /> เจ็บปวดแผล / 	
			<input name='tracking[]' type="checkbox" value="เจ็บปวดภายใน" /> เจ็บปวดภายใน / 				
			<input name='tracking[]' type="checkbox" value="ซีสหรือก้อนแข็งคลำได้" /> ซีสหรือก้อนแข็งคลำได้ / 
			<input name='tracking[]' type="checkbox" value="มีก้อนเนื้อ" /> มีก้อนเนื้อ / 			
			<input name='tracking[]' type="checkbox" value="มีผื่น" /> มีผื่น / 
			<input name='tracking[]' type="checkbox" value="ฝี" /> ฝี / 
			<input name='tracking[]' type="checkbox" value="คัน" /> คัน / 
			<input name='tracking[]' type="checkbox" value="ไหล่ติดนิ้วติด" /> ไหล่ติดนิ้วติด / 
			<br/>
			<input name='tracking[]' type="checkbox" value="เจ็บท้อง-ปวดท้อง" /> เจ็บท้อง-ปวดท้อง / 			
			<input name='tracking[]' type="checkbox" value="ท้องอืด-ท้องเฟ้อ" /> ท้องอืด-ท้องเฟ้อ / 
			<input name='tracking[]' type="checkbox" value="ท้องเสีย" /> ท้องเสีย / 
			<input name='tracking[]' type="checkbox" value="ท้องผูก" /> ท้องผูก / 
			<input name='tracking[]' type="checkbox" value="แน่นท้อง" /> แน่นท้อง / 			
			<input name='tracking[]' type="checkbox" value="ถ่ายเป็นเลือด" /> ถ่ายเป็นเลือด / 	
			<br/>		
			<input name='tracking[]' type="checkbox" value="ปัสสาวะบ่อย" /> ปัสสาวะบ่อย / 
			<input name='tracking[]' type="checkbox" value="ปัสสาวะขัด" /> ปัสสาวะขัด / 			
			<input name='tracking[]' type="checkbox" value="กลั้นปัสสาวะไม่อยู่" /> กลั้นปัสสาวะไม่อยู่ / 	
			<br/>
			<input name='tracking[]' type="checkbox" value="ทานอาหารไม่ลง-เบื่ออาหาร" /> ทานอาหารไม่ลง-เบื่ออาหาร / 
			<input name='tracking[]' type="checkbox" value="นอนไม่หลับ" /> นอนไม่หลับ / 
			<input name='tracking[]' type="checkbox" value="น้ำหนักลด" /> น้ำหนักลด / 
			<br/>
			<input name='tracking[]' type="checkbox" value="มือเท้าชา" /> มือเท้าชา / 
			<input name='tracking[]' type="checkbox" value="ตัวบวม" /> ตัวบวม  / 
			<input name='tracking[]' type="checkbox" value="แขนบวม" /> แขนบวม  / 
			<input name='tracking[]' type="checkbox" value="ขาบวม" /> ขาบวม  / 
			<input name='tracking[]' type="checkbox" value="ตัวเหลือง-ตาเหลือง" /> ตัวเหลือง-ตาเหลือง  / 
			<br/>
			<input name='tracking[]' type="checkbox" value="การกลืนติดขัด" /> การกลืนติดขัด / 
			<input name='tracking[]' type="checkbox" value="แสบร้อนกลางอก" /> แสบร้อนกลางอก / 			
			<br/>
			<input name='tracking[]' type="checkbox" value="หน้ามืดวิงเวียน" /> หน้ามืดวิงเวียน / 			
			<input name='tracking[]' type="checkbox" value="อาเจียร" /> อาเจียร / 
			<input name='tracking[]' type="checkbox" value="ปวดหัว" /> ปวดหัว / 
			<input name='tracking[]' type="checkbox" value="ปวดเมื่อย" /> ปวดเมื่อย / 			
			<br/>	
			<input name='tracking[]' type="checkbox" value="ไข้" /> ไข้ / 	
			<input name='tracking[]' type="checkbox" value="หวัด" /> หวัด / 	
			<input name='tracking[]' type="checkbox" value="น้ำมูก" /> น้ำมูก / 
			<input name='tracking[]' type="checkbox" value="เสมหะ" /> เสมหะ / 				
			<input name='tracking[]' type="checkbox" value="ไอ-จาม" /> ไอ-จาม / 			
			<input name='tracking[]' type="checkbox" value="เหนื่อยง่าย" /> เหนื่อยง่าย / 
			<input name='tracking[]' type="checkbox" value="อ่อนเพลีย-ไม่มีแรง" /> อ่อนเพลีย-ไม่มีแรง / 
			<input name='tracking[]' type="checkbox" value="ง่วงตลอดเวลา" /> ง่วงตลอดเวลา / 
			<br/>
			<?= Form::textarea('additional', '', array('placeholder' => 'อาการเพิ่มเติม','style' => 'height:10em;') ) ?>	
			<br/>
			<center>												
			<?= Form::submit('Keep Tracking', array('class'=>'small button')) ?>
			</center>
			<?= Form::close() ?>			
		</div>  
		<br/>
	</div> 

@stop