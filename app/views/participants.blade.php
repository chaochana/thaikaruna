@extends('layouts.master960widthnofixedtop')

@section('css')
	<!--
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/smoothness/jquery-ui.css">
	-->
	<link rel="stylesheet" type="text/css" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/themes/smoothness/jquery-ui.min.css">
	
	<link href="{{ URL::asset('') }}css/jquery.tagit.css" rel="stylesheet" type="text/css">             
@stop

@section('js')
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!--
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	-->
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
	
	<script src="{{ URL::asset('') }}js/tag-it.js" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript">
	    $(document).ready(function() {
	        //$('input[name="tags"]').tagit();
	        $("#myTags").tagit({
				readOnly: true
			});
	    });
	</script>

@stop

@section('header')

@stop


@section('content')

	<div class="row" style="padding-top:20px;">
		<div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns">
		<h1>Participants {{ $group }}</h1>
		<?php $count = 1; ?>
		<ol>
		@foreach ($participants as $participant)
			<li>
			{{ "<a href='".URL::asset('')."view/".$participant->MemberID."''><u>" }}
			{{ $count.". ".$participant->Title." ".$participant->Name." ".$participant->LastName." (".$participant->MemberID.")</u>" }}
			</a>
			{{ " - ".$participant->Symptom1." ".$participant->Symptom2." ".$participant->Symptom3." ".$participant->Symptom4." ".$participant->Symptom5 }}
			</li>
			<?php $count++; ?>
		@endforeach
		</ol>
		</div>  
		<br/>
	</div> 

@stop